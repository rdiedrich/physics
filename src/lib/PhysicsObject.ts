import * as PIXI from "pixi.js"
import * as p2 from "p2"

export class PhysicsObject {
  body
  shape
  graphics
  alive = true
  world?: p2.World

  constructor(body: p2.Body, shape: p2.Box, graphics: PIXI.Graphics) {
    this.body = body
    this.shape = shape
    this.graphics = graphics

    this.body.addShape(this.shape)

    this.graphics.interactive = true
    this.graphics.on("click", (_e) => {
      this.body.applyImpulse([0, 25])
    })
  }

  assign(world: p2.World, container: PIXI.Container) {
    this.world = world
    world.addBody(this.body)
    container.addChild(this.graphics)
  }

  destroy() {
    this.graphics.parent.removeChild(this.graphics)
    this.graphics.destroy()
    this.world!.removeBody(this.body)

    this.alive = false
  }

  update() {
    if (!this.alive) {
      return
    }

    // Transfer positions of the physics objects to Pixi.js
    this.graphics.position.x = this.body.position[0]
    this.graphics.position.y = this.body.position[1]
    this.graphics.rotation = this.body.angle

    if (this.graphics.position.y < -25) {
      this.destroy()
    }
  }
}
