import * as PIXI from "pixi.js"
import * as p2 from "p2"
import "./style.css"
import { PhysicsObject } from "./lib/PhysicsObject"

const vw = Math.max(
  document.documentElement.clientWidth || 0,
  window.innerWidth || 0
)
const vh = Math.max(
  document.documentElement.clientHeight || 0,
  window.innerHeight || 0
)
let zoom = 15

function randomInt(max: number) {
  return Math.floor(Math.random() * (max + 1))
}

function makeApp() {
  let app = new PIXI.Application({
    width: vw,
    height: vh,
    antialias: true,
    backgroundColor: 0xffffff
  })
  document.body.appendChild(app.view)
  return app
}

function makeBox(
  mass = 1,
  x = 0,
  y = 2,
  width = 2,
  height = 1,
  color = 0xff0000
) {
  let body = new p2.Body({
      mass: mass,
      position: [x, y],
      angularVelocity: mass === 0 ? 0 : 1
    }),
    shape = new p2.Box({
      width: width,
      height: height
    }),
    graphics = new PIXI.Graphics()

  let box = new PhysicsObject(body, shape, graphics)
  box.graphics.beginFill(color)
  box.graphics.drawRect(
    -box.shape.width / 2,
    -box.shape.height / 2,
    box.shape.width,
    box.shape.height
  )
  box.graphics.endFill()
  box.assign(world, container)
  worldObjects.push(box)

  return box
}

function makeContainer() {
  let container = new PIXI.Container()

  container.interactive = true
  container.position.x = app.renderer.width / 2
  container.position.y = app.renderer.height / 2 + 50
  container.scale.x = zoom // zoom in
  container.scale.y = -zoom // flip the y axis for physics

  return container
}

function makeFloor() {
  // zero mass box == static
  return makeBox(0, 0, -2, 50, 0.2, 0x666666)
}

function makeRandomBox() {
  const width = 1 + Math.random() * 3,
    height = 1 + Math.random() * 3,
    mass = 0.5 * Math.sqrt(width * height),
    position = [randomInt(25) - 12.5, randomInt(10) + 5],
    color = randomInt(0xffffff)

  let box = makeBox(mass, position[0], position[1], width, height, color)
  box.body.angularVelocity = Math.random() * 2

  return box
}

const app = makeApp()

let worldObjects: PhysicsObject[] = []

let world = new p2.World({ gravity: [0, -9.81] })

// Add a plane

// let planeShape = new p2.Plane()
// let planeBody = new p2.Body({
//   position: [0, -2]
// })
// planeBody.addShape(planeShape)
// world.addBody(planeBody)

let container = makeContainer()
app.stage.addChild(container)

// Add a box
makeBox()
// static ground box
makeFloor()

app.ticker.add((_delta) => {
  // cleanup dead objects
  if (worldObjects.length > 100) {
    worldObjects = worldObjects.filter((o) => o.alive)
    console.log(worldObjects.length)
  }

  world.step(1 / 60)
  worldObjects.forEach((o) => o.update())
})

function doZoom(e: WheelEvent) {
  zoom += -e.deltaY * 0.05
  zoom = Math.min(Math.max(1, zoom), 100)
  container.scale.x = zoom // zoom in
  container.scale.y = -zoom // Note: we flip the y axis to make "up" the physics
}
window.addEventListener("wheel", doZoom)
window.addEventListener("click", makeRandomBox)
window.addEventListener("touchstart", makeRandomBox)

window.setInterval(() => {
  makeRandomBox()
}, 100)
